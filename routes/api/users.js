const express = require('express');
const router = express.Router();

const gravatar = require('gravatar');
const jwt = require('jsonwebtoken');
const config = require('config');
const {check, validationResult} = require('express-validator');
const bcrypt = require('bcryptjs')
const User = require('../../model/User');

//@route  POST api/users
//@desc   create new user
//@access PUBLIC 
router.post('/',[

check('name', 'name is required').not().isEmpty(),
check('email','Please include a valid email').isEmail(),
check('password','Please enter a password with 6 or more character').isLength({min:6})
// check('phoneNumber','Not a valid phone-number').isMobilePhone() 
],

async (req,res)=>{
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array() });
    }

    const {name,email,password} = req.body;

    try{
        //see if user exists
            let user = await User.findOne({email})

            if (user) {
                res.status(400).json({error: [{msg:"User Already Exist"}]});
            }
            
            else if (!user){    //Get users gravatar
                const avatar = gravatar.url(email, {
                    s: '200',
                    r:'pg',
                    d:'mm'
                })

                user = new User({
                    name,
                    email,
                    avatar,
                    password
                })
                //hash password
                const salt = await bcrypt.genSalt(10);
                user.password = await bcrypt.hash(password,salt);
                await user.save();
                // res.send('user registered!');
                
                
                //return jsonwebtoken(when user sign up, he/she gets logged in right away)
                const payload = {
                    user: {
                        id:user.id
                    }
                }

                jwt.sign(payload, config.get('jwtToken'), {expiresIn:360000},(err,token)=>{
                    if (err) throw err;
                    res.send({token});
                });
                }




    } catch(err){
        console.error(err.message);
    }
})


module.exports = router;